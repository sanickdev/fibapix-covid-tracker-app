import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";

export default function RecordsScreen(props) {

  const { navigation } = props;

  return (
    <View style={styles.container}>
      <Text style={styles.today}>Today</Text>
      <Text style={styles.loremIpsum}>
        06/11/2021{"\n"}
        {"\n"}[Employee&#39;s name] - Negative
      </Text>
      <View style={styles.rect}></View>
      <Text style={styles.lastWeek}>Last week</Text>
      <Text style={styles.loremIpsum1}>
        05/30/2021{"\n"}
        {"\n"}Negative
      </Text>
      <View style={styles.rect1}></View>
      <View style={styles.rect2}></View>
      <Text style={styles.loremIpsum2}>
        05/23/2021{"\n"}
        {"\n"}Negative
      </Text>
      <Text style={styles.pastWeeks}>Past weeks</Text>
      <Text style={styles.loremIpsum3}>
        05/16/2021{"\n"}
        {"\n"}Negative
      </Text>
      <View style={styles.rect3}></View>

      <View style={[stylesFooter.container, props.style]}>
        <TouchableOpacity style={stylesFooter.btnWrapper1} onPress={ () => navigation.navigate('CameraScreen') }>
          <MaterialCommunityIconsIcon name={"format-line-weight"} style={stylesFooter.icon1} ></MaterialCommunityIconsIcon>
          <Text style={stylesFooter.btn1Text}>Fill test</Text>
        </TouchableOpacity>
        <TouchableOpacity style={stylesFooter.btnWrapper3} onPress={ () => navigation.navigate('RecordsScreen') }>
          <MaterialCommunityIconsIcon name={"view-list"} style={stylesFooter.icon3} ></MaterialCommunityIconsIcon>
          <Text style={stylesFooter.btn3Text}>Records</Text>
        </TouchableOpacity>
      </View>
    </View>
  );

}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    today: {
      color: "#121212",
      height: 21,
      width: 45,
      marginTop: 67,
      marginLeft: 33
    },
    loremIpsum: {
      color: "#121212",
      height: 53,
      width: 277,
      marginTop: 24,
      marginLeft: 33
    },
    rect: {
      width: 295,
      height: 1,
      backgroundColor: "#E6E6E6",
      marginTop: 16,
      marginLeft: 33
    },
    lastWeek: {
      color: "#121212",
      height: 21,
      width: 65,
      marginTop: 18,
      marginLeft: 33
    },
    loremIpsum1: {
      color: "#121212",
      height: 53,
      width: 277,
      marginTop: 23,
      marginLeft: 33
    },
    rect1: {
      width: 295,
      height: 1,
      backgroundColor: "#E6E6E6",
      marginTop: 18,
      marginLeft: 33
    },
    rect2: {
      width: 295,
      height: 1,
      backgroundColor: "#E6E6E6",
      marginTop: 130,
      marginLeft: 33
    },
    loremIpsum2: {
      color: "#121212",
      height: 53,
      width: 277,
      marginTop: -72,
      marginLeft: 33
    },
    pastWeeks: {
      color: "#121212",
      height: 21,
      width: 89,
      marginTop: -97,
      marginLeft: 33
    },
    loremIpsum3: {
      color: "#121212",
      height: 53,
      width: 277,
      marginTop: 114,
      marginLeft: 33
    },
    rect3: {
      width: 295,
      height: 243,
      marginTop: 18,
      marginLeft: 33
    }
  });

  const stylesFooter = StyleSheet.create({
    container: {
      backgroundColor: "rgba(252,201,69,1)",
      flexDirection: "row",
      alignItems: "center",
      shadowColor: "#111",
      shadowOffset: {
        width: 0,
        height: -2
      },
      flex: 1
    },
    btnWrapper1: {
      flex: 1,
      paddingTop: 8,
      paddingBottom: 6,
      paddingHorizontal: 12,
      minWidth: 80,
      maxWidth: 168,
      alignItems: "center"
    },
    icon1: {
      backgroundColor: "transparent",
      color: "#FFFFFF",
      fontSize: 24,
      opacity: 0.8
    },
    btn1Text: {
      color: "#FFFFFF",
      opacity: 0.8
    },
    btnWrapper3: {
      flex: 1,
      paddingTop: 8,
      paddingBottom: 6,
      paddingHorizontal: 12,
      minWidth: 80,
      maxWidth: 168,
      alignItems: "center"
    },
    icon3: {
      backgroundColor: "transparent",
      color: "#FFFFFF",
      fontSize: 24,
      opacity: 0.8
    },
    btn3Text: {
      color: "#FFFFFF",
      opacity: 0.8
    }
  });