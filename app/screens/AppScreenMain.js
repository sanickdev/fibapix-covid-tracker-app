import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image, Alert } from 'react-native';
import React, { Component } from 'react';
import Button from '../components/ui/button/Button';
import Textbox from '../components/ui/textbox/Textbox';
import IconTextbox from '../components/ui/icontextbox/IconTextbox';
import axios from 'axios';
import { COREENDPOINT } from '@env'
//import MaterialSwitch1 from "../components/MaterialSwitch1";


class AppScreenMain extends Component {

  constructor(props) {
    super(props);
  }

  state = {
    name: "",
    number: ""
  }


  componentDidMount() {
    console.log("2")
    /*
    axios.get('/orders.json').then(res => {
      this.setState( {loading: false, orders: fetchedOrders} );
    })
    .catch(err => {
      this.setState( {loading: false} );
    });
    */

    
  }

  userNotFoundAlert = () => {
    Alert.alert("Error", "The user you tried to access wasn't found", [
      { text: "Cancel", onPress: () => console.log("Cancel Pressed"), style: "cancel" },
      { text: "OK", onPress: () => console.log("OK Pressed") }
    ]);
  }

  

  submitHandler = () => {
    axios.post(COREENDPOINT + '/login', { security: { employeeNo: this.state.name, socialSecurityNo: this.state.number } } ).then( response => {
      console.log("response--> ", response.data)
      if(response.data.found){
        this.props.navigation.navigate('InitScreen', {
          name: this.state.name,
          number: this.state.number
        })
      }else{
        this.userNotFoundAlert();
      }
    })
    .catch(error => {
        console.log(error);
    });
  }

  updateEmployeeName = (value) => {
    this.setState({name : value});
  }

  updateEmployeeNumber = (value) => {
    this.setState({number : value});
  }


  render() {
    let disable = (this.state.name ? false : true) || (this.state.number ? false : true);
    return (
    <View style={styles.container}>
      <Button style={styles.materialButtonViolet} onPress={ this.submitHandler } name="SUBMIT" disabled={disable}> </Button>
      <View style={styles.keepSesionOpenRow}>
        {/*
        <Text style={styles.keepSesionOpen}>Keep sesion open</Text>
        <MaterialSwitch1 style={styles.materialSwitch1}></MaterialSwitch1>
        */}
      </View>
      
      {/*<Text style={styles.poweredByCompan}> Powered by: [Company&#39;s name + logo] </Text>*/}
      <Text style={styles.poweredByCompan}> Powered by: Fibapix </Text>
      <Image source={require("../assets/images/logoWText.png")} resizeMode="contain" style={styles.image} ></Image>
      <Textbox style={styles.materialUnderlineTextbox1} placeholder="Employee number" onChangeText={this.updateEmployeeName} value={this.state.name}  ></Textbox>
      <IconTextbox style={styles.materialRightIconTextbox2} placeholder="Last four social security numbers" onChangeText={this.updateEmployeeNumber} value={this.state.number} ></IconTextbox>
    </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  materialButtonViolet: {
    height: 36,
    width: 100,
    marginTop: 612,
    marginLeft: 138
  },
  keepSesionOpen: {
    color: "#121212"
  },
  materialSwitch1: {
    width: 45,
    height: 23,
    marginLeft: 174
  },
  keepSesionOpenRow: {
    height: 23,
    flexDirection: "row",
    marginTop: -163,
    marginLeft: 23,
    marginRight: 23
  },
  poweredByCompan: {
    color: "#121212",
    height: 24,
    width: 324,
    opacity: 0.32,
    marginTop: 213,
    marginLeft: 28
  },
  image: {
    width: 200,
    height: 200,
    marginTop: -702,
    marginLeft: 90
  },
  materialUnderlineTextbox1: {
    height: 43,
    width: 324,
    marginTop: 33,
    marginLeft: 28
  },
  materialRightIconTextbox2: {
    height: 43,
    width: 324,
    marginTop: 53,
    marginLeft: 28
  }
});

export default AppScreenMain;