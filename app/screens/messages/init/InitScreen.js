import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity, Image } from "react-native";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";
import Svg, { Circle } from 'react-native-svg';

class InitScreen extends Component {

  state = {
    name: "",
    number: ""
  }

  constructor(props) {
    super(props);
    this.state = {...props.route.params};
  }

  componentDidMount() {
    //TO DO
    console.log("asdasd->", this.state)
  }

  submitCameraHandler = () => {
    this.props.navigation.navigate('CameraScreen', {
      ...this.state
    });
  }

  submitRecordsHandler = () => {
    this.props.navigation.navigate('RecordsScreen', {
      ...this.state
    });
  }

  submitLogoutHandler = () => {
    this.props.navigation.navigate('AppScreenMain', {
      ...this.state
    });
  }

  render() {
    //#2e84bc   That blue
    return (
      <View style={styles.container}>
        <Svg height="92%" width="100%">
          <Circle cx="100" cy="30" r="10" fill="#FFFFFF" />
          <Circle cx="200" cy="20" r="10" fill="#FFFFFF" />
          <Circle cx="300" cy="30" r="10" fill="#FFFFFF" />
          <Text style={styles.loremIpsum}>
          Hello {this.state.name},{"\n"}Welcome to the Oil Changers&#39; Covid-19
          Employee Test Tracker. To continue you agree to one of the following
          statements:{"\n"}If you are Unvaccinated - you agree that you are taking
          the Fed/State OSHA-ETS Mandatory Covid-19 weekly test while you are
          clocked in for work in PMA and will be observed taking the test by the
          store management.{"\n"}
          {"\n"}If you are fully vaccinated for more thant 14 days as of today and
          have submitted the Vaccination Card to HR Department you do not need to
          take the weekly Covid-19 test.
        </Text>
        <View style={styles.loremIpsumFiller}></View>
          <Circle cx="100" cy="600" r="10" fill="#FFFFFF" />
          <Circle cx="200" cy="610" r="10" fill="#FFFFFF" />
          <Circle cx="300" cy="600" r="10" fill="#FFFFFF" />
          <Image source={require("../../../assets/images/covidKid.png")} resizeMode="contain" style={styles.imageKid} ></Image>
        </Svg>
        <View style={[stylesFooter.container, this.props.style]}>
          <TouchableOpacity style={stylesFooter.btnWrapper1} onPress={ this.submitCameraHandler }>
            <MaterialCommunityIconsIcon name={"format-line-weight"} style={stylesFooter.icon1} ></MaterialCommunityIconsIcon>
            <Text style={stylesFooter.btn1Text}>Fill test</Text>
          </TouchableOpacity>
          <TouchableOpacity style={stylesFooter.btnWrapper3} onPress={ this.submitLogoutHandler }>
            <MaterialCommunityIconsIcon name={"view-list"} style={stylesFooter.icon3} ></MaterialCommunityIconsIcon>
            <Text style={stylesFooter.btn3Text}>Logout</Text>
          </TouchableOpacity>
        </View>
      </View>
    );

  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#409cce"
  },
  loremIpsum: {
    color: "#FFFFFF",
    height: 571,
    width: 327,
    fontSize: 18,
    textAlign: "justify",
    marginTop: 62,
    marginLeft: 23
  },
  loremIpsumFiller: {
    flex: 1
  },
  imageKid: {
    width: 340,
    height: 340,
    marginTop: -215,
    marginLeft: 0
  },
  materialBasicFooter: {
    height: 56,
    width: 412,
    alignSelf: "flex-end",
    marginRight: -1
  }
});


const stylesFooter = StyleSheet.create({
  container: {
    backgroundColor: "rgba(252,201,69,1)",
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "#111",
    shadowOffset: {
      width: 0,
      height: -2
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.2,
    elevation: 3
  },
  btnWrapper1: {
    flex: 1,
    paddingTop: 8,
    paddingBottom: 6,
    paddingHorizontal: 12,
    minWidth: 80,
    maxWidth: 168,
    alignItems: "center"
  },
  icon1: {
    backgroundColor: "transparent",
    color: "#FFFFFF",
    fontSize: 24,
    opacity: 0.8
  },
  btn1Text: {
    color: "#FFFFFF",
    opacity: 0.8
  },
  btnWrapper3: {
    flex: 1,
    paddingTop: 8,
    paddingBottom: 6,
    paddingHorizontal: 12,
    minWidth: 80,
    maxWidth: 168,
    alignItems: "center"
  },
  icon3: {
    backgroundColor: "transparent",
    color: "#FFFFFF",
    fontSize: 24,
    opacity: 0.8
  },
  btn3Text: {
    color: "#FFFFFF",
    opacity: 0.8
  }
});

export default InitScreen;