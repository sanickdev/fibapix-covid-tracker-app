import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import MaterialButtonViolet3 from "../../../components/testconfirmed/MaterialButtonViolet3";

export default function TestConfirmedScreen(props) {

  const { navigation } = props;

  return (
    <View style={styles.container}>
      <Text style={styles.loremIpsum}>
        Thank you for taking the Weekly Mandatory test, to avoid taking the
        weekly tests, get Fully Vaccinated by going to your local pharmacy,
        getting vaccinated is free.{"\n"}
        {"\n"}Please continue to follow all our Covid-19 safety protocols- see
        &quot;OSHA- ETS Information&quot; within the menu tab
      </Text>
      <MaterialButtonViolet3 style={styles.materialButtonViolet3} onPress={ () => navigation.navigate('AppScreenMain') } ></MaterialButtonViolet3>
    </View>
  );

}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    loremIpsum: {
      color: "#121212",
      height: 401,
      width: 297,
      fontSize: 20,
      marginTop: 96,
      marginLeft: 60
    },
    materialButtonViolet3: {
      height: 36,
      width: 120,
      marginTop: 136,
      marginLeft: 128
    }
});