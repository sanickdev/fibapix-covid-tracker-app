import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
//Custom
import Textbox from "../../../components/ui/textbox/Textbox";
import IconTextbox from "../../../components/ui/icontextbox/IconTextbox";
import Button from "../../../components/ui/button/Button";
import RadioButton from "../../../components/ui/radiobutton/RadioButton";
//External Libraries
import CalendarPicker from 'react-native-calendar-picker';

class RecordTestScreen extends Component {

  

  state = {
    name: "",
    number: "",
    camera: null,
    date: "",
    selectedStartDate: null,
    resultCovidNeg: false,
    resultCovidPos: false
  }

  constructor(props) {
    super(props);
    this.state = {...props.route.params, selectedStartDate: null};
    this.onDateChange = this.onDateChange.bind(this);
  }

  onDateChange(value) {
    let dateString = JSON.stringify(value).replace('"', '');
    let result = dateString.slice(0, dateString.length - 15);
    this.setState({ selectedStartDate: value, date: result });
  }

  componentDidMount() {
  }

  submitHandler = () => {
    this.props.navigation.navigate('TestConfirmedScreen', {
      ...this.state
    })
  }

  onChangeCovidResult = (value) => {
    this.setState({resultCovidPos: value, resultCovidNeg: !value});
  }

  render() {
    const { selectedStartDate } = this.state;
    const startDate = selectedStartDate ? selectedStartDate.toString() : '';
    return (
      <View style={styles.container}>
        <Textbox style={styles.materialDisabledTextbox} placeholder="Name (By default)" value={this.state.name} editable={false}  ></Textbox>
        <IconTextbox style={styles.materialDisabledTextbox} placeholder="Date" iconName="calendar" value={this.state.date}  ></IconTextbox>
        <View style={styles.calendarPicker}>
          <CalendarPicker onDateChange={this.onDateChange} />
        </View>
        <View style={styles.materialRadioRow}>
          <RadioButton style={styles.materialRadio} selected={this.state.resultCovidPos} onPress={ () => this.onChangeCovidResult(true) } ></RadioButton>
          <Text style={styles.positive}>Positive</Text>
        </View>
        <View style={styles.materialRadio1Row}>
          <RadioButton style={styles.materialRadio1} selected={this.state.resultCovidNeg} onPress={ () => this.onChangeCovidResult(false) } ></RadioButton>
          <Text style={styles.negative}>Negative</Text>
        </View>
        <View style={styles.resultCovidTestRow}>
          <Text style={styles.resultCovidTest}>COVID-19 Test results</Text>
        </View>
        <Button style={styles.submitButton} onPress={ this.submitHandler } name="SUBMIT" ></Button>
      </View>
    );
  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  calendarPicker: {
    marginTop: 7,
    color: 'blue'
  }
  , materialDisabledTextbox: {
    height: 43,
    width: 304,
    marginTop: 5,
    marginLeft: 38
  },
  materialRadio: {
    height: 40,
    width: 40
  },
  positive: {
    color: "#121212",
    height: 18,
    width: 56,
    marginTop: 11
  },
  materialRadioRow: {
    height: 40,
    flexDirection: "row",
    marginTop: 75,
    marginLeft: 38,
    marginRight: 241
  },
  materialRadio1: {
    height: 40,
    width: 40
  },
  negative: {
    color: "#121212",
    height: 20,
    width: 66,
    marginTop: 11
  },
  materialRadio1Row: {
    height: 40,
    flexDirection: "row",
    marginLeft: 38,
    marginRight: 231
  },
  resultCovidTest: {
    color: "#121212",
    height: 26,
    width: 160
  },
  resultCovidTestRow: {
    height: 0,
    flexDirection: "row",
    marginTop: -123,
    marginLeft: 38,
    marginRight: 183
  },
  submitButton: {
    height: 36,
    width: 100,
    marginTop: 188,
    marginLeft: 142
  }
});

export default RecordTestScreen;