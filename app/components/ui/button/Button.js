import React, { Component } from "react";
import { StyleSheet, TouchableOpacity, Text, View } from "react-native";

function Button(props) {
  return (
    <View style={{opacity: props.disabled ? .7 : 1}}>
      <TouchableOpacity style={[styles.container, props.style]} onPress={props.onPress} disabled={props.disabled}>
        <Text style={styles.login}>{props.name}</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "rgba(255,203,64,1)",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    borderRadius: 2,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.35,
    shadowRadius: 5,
    minWidth: 88,
    paddingLeft: 16,
    paddingRight: 16
  },
  login: {
    color: "#fff",
    fontSize: 14
  }
});

export default Button;
