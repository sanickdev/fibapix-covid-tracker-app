import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
//Screens
import AppScreenMain from './app/screens/AppScreenMain';
import InitScreen from './app/screens/messages/init/InitScreen';
import CameraScreen from './app/screens/filltest/camera/CameraScreen';
import RecordTestScreen from './app/screens/filltest/recordtest/RecordTestScreen';
import TestConfirmedScreen from './app/screens/messages/testconfirmed/TestConfirmedScreen';
import RecordsScreen from './app/screens/catalog/records/RecordsScreen';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
  <NavigationContainer navigationOptions={{ headerShown: false }}>
    <Stack.Navigator initialRouteName="AppScreenMain">
      <Stack.Screen name="AppScreenMain" component={AppScreenMain} options={{ title: '', headerShown: false }} />
      <Stack.Screen name="InitScreen" component={InitScreen} options={{ title: '' }} />
      <Stack.Screen name="CameraScreen" component={CameraScreen} options={{ title: '', headerShown: false }} />
      <Stack.Screen name="RecordTestScreen" component={RecordTestScreen} options={{ title: '', headerShown: true }} />
      <Stack.Screen name="TestConfirmedScreen" component={TestConfirmedScreen} options={{ title: '', headerShown: false }} />
      <Stack.Screen name="RecordsScreen" component={RecordsScreen} options={{ title: '', headerShown: false }} />
    </Stack.Navigator>
  </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
